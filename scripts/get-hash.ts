import * as dotenv from 'dotenv-flow';
dotenv.config();

import hash from '../src/encryption';

const args = process.argv.slice(2);
console.log('Args:', args);

(async function () {
    for (const arg of args) {
        console.log('AES-256 cipher for', arg, '-->', await hash(arg));
    }
})();
