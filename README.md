# Sorbot

> Le bot du réseau de serveurs Discord étudiant **Sorbonne Jussieu**, faculté des sciences de Sorbonne Université.

## Statut du bot

En bon fonctionnement depuis 2020 !

## Fonctionnalités

Sorbot a été conçu en premier lieu pour vérifier les membres universitaires dans l'ensemble de sa fédération de serveurs Discord.
Voici ses fonctionnalités :

- vérification des membres par envoi d'un mail de confirmation universitaire
- synchronisation des rôles entre serveurs fédérés
- messages d'au-revoir aléatoires pour le serveur Sorbonne Jussieu
- gestion automatique de suggestions de messages d'au-revoir

## Tech Stack

Voici les technologies utilisées :

- [Typescript](https://www.typescriptlang.org/)
- [Yarn](https://yarnpkg.com/)
- [Node.js](https://nodejs.org/)
  - [Sequelize](https://sequelize.org/) (database ORM)
  - [Discord.js](https://discord.js.org/)
- [Postgresql](https://www.postgresql.org/)

## License

Tout le code de Sorbot est sous license GPL v3. Détails dans le fichier [LICENSE.md](/LICENSE.md).
