import {
    ButtonBuilder,
    ChatInputCommandInteraction,
    Client,
    Interaction,
    InteractionType,
    MessageComponentInteraction,
    ModalBuilder,
    ModalSubmitInteraction,
    SlashCommandBuilder,
    SlashCommandOptionsOnlyBuilder,
    SlashCommandSubcommandsOnlyBuilder,
    Snowflake,
    StringSelectMenuBuilder,
    TextChannel,
} from 'discord.js';

/** A Slash Command subhandler */
export interface CommandSubhandler {
    /** Slash command metadata */
    data:
        | SlashCommandBuilder
        | SlashCommandSubcommandsOnlyBuilder
        | SlashCommandOptionsOnlyBuilder;
    /** Function to call when receiving the command */
    run: (interaction: ChatInputCommandInteraction) => Promise<void>;
}

/** Map of command subhandlers. */
export type CommandSubhandlers = {
    [key in string]?: CommandSubhandler;
};

/** A Component subhandler */
export interface ComponentSubhandler {
    /** Component metadata */
    data: ButtonBuilder | StringSelectMenuBuilder;
    /** Function to call when receiving the component response */
    run: (interaction: MessageComponentInteraction) => Promise<void>;
}

export type ComponentSubhandlers = {
    [key in string]?: ComponentSubhandler;
};

/** A Modal subhandler */
export interface ModalSubhandler {
    /** Modal metadata */
    data: ModalBuilder;
    /** Function to call when receiving the modal submit */
    run: (interaction: ModalSubmitInteraction) => Promise<void>;
}

export type ModalSubhandlers = {
    [key in string]?: ModalSubhandler;
};

export type InteractionHandler<T extends Interaction = Interaction> = (
    interaction: T
) => Promise<void>;

export type InteractionHandlers = Map<InteractionType, InteractionHandler>;

export interface StringAssociations {
    [key: string]: string;
}

/** A log channel identifier. */
export type LogChannelName =
    | 'init'
    | 'email'
    | 'database'
    | 'messages'
    | 'moderation'
    | 'wtf'
    | 'warning'
    | 'interactions';

/** A map of log channels. */
export type LogChannels<T = TextChannel | undefined> = {
    [id in LogChannelName]: T;
};

/** Object caching the client and some log channels for the logging functions. */
export interface LogsCache {
    client: Client | undefined;
    channels: LogChannels;
}

/** Snowflakes of the logging guild and all its logging channels. */
export interface LogSnowflakes {
    /** Snowflake of the logging guild. */
    guild: Snowflake;
    /** Logging channels to use in development. */
    localChannels: LogChannels<Snowflake>;
    /** Logging channels to use in production. */
    channels: LogChannels<Snowflake>;
}
