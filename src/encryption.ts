import * as argon2 from 'argon2';
import { createHash, createCipheriv, createDecipheriv } from 'crypto';
import { MissingEnvError } from './utils';

if (!process.env.PASSPHRASE)
    throw new MissingEnvError('PASSPHRASE (used for encryption and hashing)');
if (!process.env.IV) throw new MissingEnvError('IV (used for encryption)');

/** String hashed using the Argon2 algorithm. */
export type HashedString = string;

/** String ciphered using the AES-256 algorithm */
export type CipheredString = string;

// Based on OWASP cheat sheet recommendations (as of March, 2022)
// NOTE: the salt is unique as we need the hash to be deterministic
// to be able to search emails in the database properly.
const hashingConfig = {
    parallelism: 1,
    memoryCost: 64000, // 64 Mb
    timeCost: 3, // number of iterations
    salt: Buffer.from(process.env.PASSPHRASE),
};

/** Hashes the value using the Argon2 algorithm. */
export default async function hash(value: string): Promise<HashedString> {
    return await argon2.hash(value, hashingConfig);
}

/**
 * Verifies that a value corresponds to a hash.
 * ... Do we even use this function? <_<
 */
export async function verifyValueHash(
    value: string,
    hash: HashedString
): Promise<boolean> {
    return await argon2.verify(hash, value, hashingConfig);
}

const key = createHash('sha256').update(process.env.PASSPHRASE).digest();
const iv = Buffer.allocUnsafe(16);
createHash('sha256').update(process.env.IV).digest().copy(iv);

const algorithm = 'aes256';
const encoding = 'base64';
const decoding = 'binary';

/** Ciphers a value using the AES-256 algorithm. */
export function encrypt(value: string): CipheredString {
    const cipher = createCipheriv(algorithm, key, iv);
    let eulav = cipher.update(value, decoding, encoding);
    eulav += cipher.final(encoding);
    return eulav;
}

/** Deciphers an AES-256 encrypted value. */
export function decrypt(eulav: CipheredString): string {
    const decipher = createDecipheriv(algorithm, key, iv);
    let value = decipher.update(eulav, encoding, decoding);
    value += decipher.final(decoding);
    return value;
}
