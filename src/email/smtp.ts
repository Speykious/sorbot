import { User } from 'discord.js';
import { createTransport } from 'nodemailer';
import { dislog, formatUser } from '../logging';
import { MissingEnvError, readf } from '../utils';

const { EMAIL_USER, EMAIL_PASSWORD, EMAIL_HOST, SMTP_PORT } = process.env;
if (!EMAIL_USER) throw new MissingEnvError('EMAIL_USER');
if (!EMAIL_PASSWORD) throw new MissingEnvError('EMAIL_PASSWORD');
if (!EMAIL_HOST) throw new MissingEnvError('EMAIL_HOST');
if (!SMTP_PORT) throw new MissingEnvError('SMTP_PORT');

const transporter = createTransport({
    host: EMAIL_HOST,
    port: +SMTP_PORT,
    secure: false,
    auth: {
        user: EMAIL_USER,
        pass: EMAIL_PASSWORD,
    },
});

/** Send a confirmation email to the user with a given confirmation code. */
export async function sendConfirmationEmail(
    address: string,
    user: User,
    code: string
): Promise<void> {
    const info = await transporter.sendMail({
        from: 'Sorbot <sorbot@yeetsquared.org>',
        to: address,
        subject: '[Discord Sorbonne Jussieu] Code de confirmation',
        text: readf('resources/confirmation-email.txt')
            .replace(/\{tag\}/g, user.tag)
            .replace(/\{code\}/g, code),
        html: readf('resources/confirmation-email.html')
            .replace(/\{tag\}/g, user.tag)
            .replace(/\{code\}/g, code),
    });

    dislog(
        'email',
        `Email sent to user ${formatUser(user)}\n` +
            `\`\`\`yaml\nresponse: ${info.response}\nmessageId: ${info.messageId}\n\`\`\``
    );
}
