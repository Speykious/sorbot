import EmailDomain from '../database/models/EmailDomain';

// Regex taken from https://www.w3resource.com/javascript/form/email-validation.php
// We don't need to think too much about this since the important thing is that it works for Sorbonne email addresses.
const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

/** Whether the string entered is an email address. */
export function isEmail(email: string): boolean {
    return emailRegex.test(email);
}

/** Returns the email domain of the address if it is a Sorbonne domain. */
export async function getSorbonneDomain(
    email: string
): Promise<EmailDomain | null> {
    const domain = email.split('@')[1];
    const emailDomain = await EmailDomain.findOne({
        where: { domain },
    });
    return emailDomain;
}

const codeCharset =
    '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

/** Generate a confirmation code for the verification process. */
export function generateCode(length = 6) {
    let code = '';

    for (let i = 0; i < length; i++)
        code +=
            codeCharset[
                Math.floor(Math.random() * (codeCharset.length - 0.001))
            ];

    return code;
}
