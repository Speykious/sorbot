import { Client, Collection } from 'discord.js';
import ImapConnection, { Box } from 'imap';
import { ParsedMail, simpleParser } from 'mailparser';
import { singleEmbedMessage, internalErrorEmbed } from '../embeds';
import { dislog } from '../logging';
import { MissingEnvError } from '../utils';

const { EMAIL_USER, EMAIL_PASSWORD, EMAIL_HOST, IMAP_PORT } = process.env;
if (!EMAIL_USER) throw new MissingEnvError('EMAIL_USER');
if (!EMAIL_PASSWORD) throw new MissingEnvError('EMAIL_PASSWORD');
if (!EMAIL_HOST) throw new MissingEnvError('EMAIL_HOST');
if (!IMAP_PORT) throw new MissingEnvError('IMAP_PORT');

const imap = new ImapConnection({
    user: EMAIL_USER,
    password: EMAIL_PASSWORD,
    host: EMAIL_HOST,
    port: +IMAP_PORT,
    tls: process.env.IMAP_TLS ? true : false,
});

/** Object to cache the client for usage with IMAP. */
export interface ImapCache {
    client?: Client;
}

/** Unique IMAP cache. */
export const imapCache: ImapCache = {};

/** Function that processes parsed email content. */
export type EmailHandler = (email: ParsedMail) => void | Promise<void>;
let emailHandler: EmailHandler | undefined = undefined;

/** Opens the email inbox. */
async function openInbox(): Promise<Box> {
    return new Promise((resolve, reject) => {
        imap.openBox('INBOX', false, (error, mailbox) => {
            if (error) reject(error);
            else resolve(mailbox);
        });
    });
}

/** Fetches unread email IDs from the inbox. */
async function getUnreadEmails(): Promise<number[]> {
    return new Promise((resolve, reject) => {
        imap.search(['UNSEEN'], (err, results) => {
            if (err) reject(err);
            else resolve(results);
        });
    });
}

/** Fetch unread emails from the inbox and marks them as seen. */
async function readUnseenEmails(): Promise<ParsedMail[]> {
    const unreads = await getUnreadEmails();
    if (unreads.length === 0) return [];

    const f = imap.fetch(unreads, {
        bodies: '',
        struct: true,
        markSeen: true,
    });

    const chunksMap: Collection<number, Buffer[]> = new Collection();
    const emails: ParsedMail[] = [];

    f.on('message', (msg, seqno) => {
        const chunks: Buffer[] = [];

        msg.on('body', stream => {
            stream.on('data', (chunk: Buffer) => {
                chunks.push(chunk);
            });
        });

        msg.once('end', async () => {
            chunksMap.set(seqno, chunks);
        });
    });

    return new Promise(resolve => {
        f.once('end', async () => {
            for (const chunks of chunksMap.values()) {
                const rawEmail = Buffer.concat(chunks);
                try {
                    const parsed = await simpleParser(rawEmail, {
                        skipTextToHtml: true,
                        skipImageLinks: true,
                        skipTextLinks: true,
                    });
                    emails.push(parsed);
                } catch (error) {
                    console.error("Couldn't parse email:", error);
                }
            }

            resolve(emails);
        });
    });
}

// Receive new emails
imap.on('mail', async () => {
    const emails = await readUnseenEmails();
    console.log(
        `Got ${emails.length} new ${emails.length === 1 ? 'email' : 'emails'}!`
    );

    if (emailHandler) {
        for (const email of emails) {
            emailHandler(email);
        }
    }
});

// Dislog on IMAP error
imap.once('error', (error: Error) => {
    dislog('wtf', singleEmbedMessage(internalErrorEmbed('IMAP', error)));
});

// ...Maybe connection should actually never end?
imap.once('end', () => {
    console.log('IMAP connection ended');
});

/** Initialize IMAP connection. */
export async function imapInit(client: Client): Promise<void> {
    imapCache.client = client;
    imap.connect();
    return new Promise(resolve => {
        imap.once('ready', async () => {
            console.log('IMAP is ready');
            openInbox();
            resolve();
        });
    });
}

/** Sets the email handler for IMAP */
export function setEmailHandler(handler: EmailHandler) {
    emailHandler = handler;
}
