import { ParsedMail } from 'mailparser';
import EmailDomain from '../database/models/EmailDomain';
import SuUser from '../database/models/SuUser';
import {
    errorEmbed,
    singleEmbedMessage,
    successEmbed,
    warningEmbed,
} from '../embeds';
import { decrypt, encrypt } from '../encryption';
import { verifyUser } from '../handlers/helpers';
import { dislog, formatUser } from '../logging';
import { imapCache } from './imap';
import { getSorbonneDomain } from './validation';

/** Set of lines to properly identify a Delivery Status Notification. */
const dsnLines = [
    'Hi!',
    'This is the MAILER-DAEMON, please DO NOT REPLY to this email.',
    'An error has occurred while attempting to deliver a message for',
    'the following list of recipients:',
];

function matchesDsn(s: string) {
    return dsnLines.every(line => s.includes(line));
}

/** When the email address doesn't exist, and thus we receive a delivery failure notification. */
async function handleExistentialCrisis(parsedMail: ParsedMail) {
    if (!parsedMail.text) return;

    const textLines = parsedMail.text?.split('\n');
    const errorLine = textLines[7];
    const email = errorLine.split(':')[0];

    // Fetching a user with an encrypted email means the user has to be unverified
    const suUser = await SuUser.findOne({
        where: { email: await encrypt(email) },
        attributes: ['id'],
    });

    if (!suUser) {
        dislog(
            'wtf',
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Delivery failure from unknown email address')
                    .setDescription(
                        "We received a MAILER-DAEMON notification about an email delivery failure, but the email address isn't attached to any known unverified user."
                    )
            )
        );
        return;
    }

    const { client } = imapCache;
    if (!client)
        throw new Error("The IMAP cache hasn't been initialized properly");

    // Send a DM to the user since the email couldn't be sent
    const user = await client.users.fetch(decrypt(suUser.id));
    if (!user.dmChannel)
        throw new Error(`User ${formatUser(user)} does not have a DM channel`);

    await user.dmChannel.send(
        singleEmbedMessage(
            errorEmbed()
                .setTitle("Impossible d'envoyer le mail")
                .setDescription(
                    "Le mail de confirmation n'a pas pu être envoyé.\n" +
                        `Voici le message d'erreur :\n\`\`\`\n${errorLine}\n\`\`\``
                )
        )
    );

    dislog(
        'email',
        `Sent email delivery failure message to ${formatUser(user)}`
    );
}

const operationOriginCrisisRegex =
    /je ne suis pas (originaire|[aà] l('|\s)?origine) de cette op[eé]ration/i;

/** When the address has a valid Sorbonne Domain. */
async function handleManualVerification(parsedMail: ParsedMail) {
    if (!parsedMail.text) return;

    const address = parsedMail.from?.value[0].address;
    if (!address) return;

    const suUser = await SuUser.findOne({ where: { email: encrypt(address) } });

    // Handle email address misuse reports
    if (operationOriginCrisisRegex.test(parsedMail.text)) {
        if (suUser) {
            const userId = decrypt(suUser.id);
            dislog('warning', {
                content: '@everyone',
                embeds: [
                    warningEmbed()
                        .setTitle('Email address misuse report')
                        .setDescription(
                            `User <@${userId}> tried to use someone else's email address!`
                        )
                        .addFields({
                            name: 'Misused email address',
                            value: address,
                        }),
                ],
            });
        } else {
            dislog('warning', {
                content: '@everyone',
                embeds: [
                    warningEmbed()
                        .setTitle('FALSE email address misuse report')
                        .setDescription(
                            'Received an email address misuse report from an unknown user.'
                        ),
                ],
            });
        }
        return;
    }

    if (!suUser) return;

    const { client } = imapCache;
    if (!client) throw new Error("IMAP cache hasn't been initialized");

    // Handle manual verification
    // The user's Discord tag has to be included in the subject or body of the email.
    const user = await client.users.fetch(decrypt(suUser.id));
    if (
        parsedMail.subject?.includes(user.tag) ||
        parsedMail.text?.includes(user.tag)
    ) {
        await verifyUser(user, suUser);
        await user.dmChannel?.send(
            singleEmbedMessage(
                successEmbed()
                    .setTitle('Mail reçu !')
                    .setDescription('Vous êtes maintenant vérifié.e 👍')
            )
        );
    }
    // Case where unverified user sends a random invalid email is not handled.
}

/** Email handler to be fed to IMAP */
export default async function emailHandler(email: ParsedMail) {
    const address = email.from?.value[0].address;
    if (!address) return;

    if (address === 'MAILER-DAEMON@ePi3' && matchesDsn(address)) {
        await handleExistentialCrisis(email);
    } else if (await getSorbonneDomain(address)) {
        await handleManualVerification(email);
    } else {
        dislog(
            'email',
            singleEmbedMessage(
                warningEmbed()
                    .setTitle('Unhandled email')
                    .setDescription(
                        `Received unhandleable email from \`${address}\``
                    )
            )
        );
    }
}
