import { version } from '../package.json';
import { AAA } from './constants';
import {
    EmbedBuilder,
    EmbedFooterData,
    MessageCreateOptions,
} from 'discord.js';

/** Sorbot's footer. */
export const footer: EmbedFooterData = {
    text: `Sorbot v${version}`,
    iconURL: 'https://i.imgur.com/e3K2oaW.png',
};

/** Message embed with Sorbot's footer. */
export function sorbotEmbed(): EmbedBuilder {
    return new EmbedBuilder().setFooter(footer);
}

/** Sorbot embed with a Sorbot-cyan side color. */
export function infoEmbed(): EmbedBuilder {
    return sorbotEmbed().setColor('#34d9ff');
}

/** Sorbot embed with a yellow side color. */
export function warningEmbed(): EmbedBuilder {
    return sorbotEmbed().setColor('#ffee64');
}

/** Sorbot embed with a red side color. */
export function errorEmbed(): EmbedBuilder {
    return sorbotEmbed().setColor('#cc0051');
}

/** Error embed which describes an internal error. */
export function internalErrorEmbed(
    errorType: string,
    error: Error | string | unknown
): EmbedBuilder {
    const embed = errorEmbed().setTitle(`${errorType} error`);

    if (error instanceof Error) {
        embed.setDescription(`\`${error.name}\`: ${error.message}`);
        if (error.stack) {
            const cbStart = '```js\n';
            const cbEnd = '\n```';
            const maxLength = 1024 - cbStart.length - cbEnd.length;
            const stack =
                error.stack.length > maxLength
                    ? `${error.stack.slice(0, maxLength - 3)}...`
                    : error.stack;
            embed.addFields({
                name: 'Stack trace',
                value: `\`\`\`js\n${stack}\n\`\`\``,
            });
        }
    } else {
        embed.setDescription(`${error}`);
    }

    return embed;
}

/** Sorbot embed with a green side color. */
export function successEmbed(): EmbedBuilder {
    return sorbotEmbed().setColor('#00ff81');
}

/** Simple success embed with one message string. */
export function simpleSuccessEmbed(successMessage: string): EmbedBuilder {
    return successEmbed()
        .setTitle('Success! :)')
        .setDescription(successMessage);
}

/** Constructs a message with a single embed in it. */
export function singleEmbedMessage(embed: EmbedBuilder): MessageCreateOptions {
    return { embeds: [embed] };
}

/** Error embed to be sent to a user to notify them that I'm a bad developer. :v */
export function criticalErrorMessage(errorTitle: string): MessageCreateOptions {
    return singleEmbedMessage(
        errorEmbed()
            .setTitle(`${AAA.repeat(3)} ${errorTitle} ${AAA.repeat(3)}`)
            .addFields({
                name: 'Oups <:oh_fuck:676542110694113285>',
                value: "Cette erreur a été reportée automatiquement aux admins.\nRassurez-vous, aucune donnée personnelle n'a été envoyée.",
            })
    );
}
