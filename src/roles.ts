import { GuildMember, User } from 'discord.js';
import { getSuGuild } from './database/models/SuGuild';
import { getSuUser } from './database/models/SuUser';

/** Give or remove roles to a member based on its saved role tags in the database. */
export async function syncMemberRoles(member: GuildMember) {
    const suUser = await getSuUser(member.user);
    if (!suUser) return;

    const suGuild = await getSuGuild(member.guild);
    if (!suGuild) return;

    await Promise.all(
        Object.entries(suGuild.roleassocs).map(([name, role]) =>
            suUser.roletags.has(name)
                ? member.roles.add(role)
                : member.roles.remove(role)
        )
    );
}

/** Synchronize a user's roles in each of our servers. */
export async function syncRoles(user: User) {
    const suUser = await getSuUser(user);
    if (!suUser) return;

    await Promise.all(
        [...suUser.servers.values()].map(async serverId => {
            const guild = await user.client.guilds.fetch(serverId);
            const member = await guild.members.fetch(user);
            return await syncMemberRoles(member);
        })
    );
}
