/** AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */
export const AAA = '<a:AAA:795037960886026242>';

/** Constructs an error message string for display purposes. */
export const errorMessage = (error: Error | unknown): string =>
    error instanceof Error ? `\`${error.name}\` ${error.message}` : `${error}`;

/** Client snowflake - Sorbot or Sorbet. */
export const CLIENT_SNOWFLAKE = process.env.LOCAL
    ? '689806945523466284'
    : '688517881763201221';

/** Snowflake of the main Sorbonne Jussieu Discord server. */
export const SORBONNE_JUSSIEU_SNOWFLAKE = '672479260899803147';

/** Snowflake of the channel where we write bye messages. */
export const BYE_CHANNEL_SNOWFLAKE = '672502429836640267';
