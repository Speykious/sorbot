import * as dotenv from 'dotenv-flow';
dotenv.config();

import { imapInit, setEmailHandler } from './email/imap';

import {
    ActivityType,
    Client,
    GatewayIntentBits,
    Guild,
    GuildMember,
    Interaction,
    InteractionType,
    PartialGuildMember,
    TextChannel,
} from 'discord.js';

import './database/database';
import { getCommandData, onCommandInteraction } from './handlers/commands';
import { onComponentInteraction } from './handlers/components';
import { InteractionHandlers } from './interfaces';
import { BYE_CHANNEL_SNOWFLAKE, SORBONNE_JUSSIEU_SNOWFLAKE } from './constants';
import {
    dislog,
    formatGuild,
    formatUser,
    initLogs,
    logSnowflakes,
} from './logging';
import {
    criticalErrorMessage,
    internalErrorEmbed,
    infoEmbed,
    singleEmbedMessage,
} from './embeds';
import { getSuUser } from './database/models/SuUser';
import { getRandomByeMessage } from './database/models/ByeMessage';
import { onModalSubmit } from './handlers/modals';
import emailHandler from './email/emailHandler';
import { syncMemberRoles } from './roles';
import { createSuGuild } from './database/models/SuGuild';

const client = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.DirectMessages,
    ],
});

client.once('ready', async () => {
    initLogs(client);
    setEmailHandler(emailHandler);
    imapInit(client);

    if (client.application) {
        console.log('Refreshing slash commands');

        const { commands } = client.application;
        if (process.env.LOCAL)
            await commands.set(getCommandData(), logSnowflakes.guild);
        else await commands.set(getCommandData());

        console.log('Successfully reloaded slash commands');
    }

    client.user?.setActivity({
        type: ActivityType.Playing,
        name: 'with your data 👀',
        url: 'http://courage.speykious.xyz',
    });

    dislog(
        'init',
        singleEmbedMessage(
            infoEmbed()
                .setTitle('Ready to be based.')
                .setDescription(
                    'Switch on the power line, remember to put on protection.' +
                        "\nLay down your pieces and let's begin object creation." +
                        '\nFill in my data, parameters, initialization.' +
                        "\nSetup our new world and let's begin the simulation." +
                        '\nReady to be based.'
                )
        )
    );
});

const interactionHandlers: InteractionHandlers = new Map([
    [InteractionType.ApplicationCommand, onCommandInteraction],
    [InteractionType.MessageComponent, onComponentInteraction],
    [InteractionType.ModalSubmit, onModalSubmit],
]);

client.on('interactionCreate', async (interaction: Interaction) => {
    const handler = interactionHandlers.get(interaction.type);
    if (!handler) return;

    try {
        await handler(interaction);
    } catch (error) {
        dislog(
            'wtf',
            singleEmbedMessage(internalErrorEmbed('Interaction', error))
        );

        if (interaction.isCommand() || interaction.isMessageComponent()) {
            if (!(interaction.deferred || interaction.replied))
                await interaction.deferReply({ ephemeral: true });
            await interaction.editReply(
                criticalErrorMessage("ERREUR D'INTERACTION")
            );
        }
    }
});

client.on('guildMemberAdd', async (member: GuildMember) => {
    try {
        if (member.user.bot) {
            dislog(
                'moderation',
                `Bot ${formatUser(member.user)} arrived in ${formatGuild(
                    member.guild
                )}`
            );
            return;
        }

        dislog(
            'moderation',
            `User ${formatUser(member.user)} joined ${formatGuild(
                member.guild
            )}`
        );

        const suUser = await getSuUser(member.user);
        if (!suUser) return;

        suUser.addServer(member.guild.id);
        await suUser.update({ servers: suUser.servers });

        await syncMemberRoles(member);
    } catch (error) {
        dislog(
            'wtf',
            singleEmbedMessage(
                internalErrorEmbed('`guildMemberAdd` event', error)
            )
        );
    }
});

client.on(
    'guildMemberRemove',
    async (member: GuildMember | PartialGuildMember) => {
        try {
            const { user, guild } = member;
            if (user.bot) {
                dislog(
                    'moderation',
                    `Bot ${formatUser(user)} departed from ${formatGuild(
                        guild
                    )}`
                );
                return;
            }

            dislog(
                'moderation',
                `User ${formatUser(user)} left ${formatGuild(guild)}`
            );

            const suUser = await getSuUser(user);
            if (suUser) {
                suUser.removeServer(guild.id);
                if (suUser.servers.size > 0) {
                    await suUser.update({ servers: suUser.servers });
                    dislog(
                        'database',
                        `User ${formatUser(user)} left ${formatGuild(guild)}`
                    );
                } else {
                    await suUser.destroy();
                    dislog('database', `User ${formatUser(user)} removed`);
                }
            } else {
                dislog(
                    'database',
                    `Unregistered user ${formatUser(user)} left ${formatGuild(
                        guild
                    )}`
                );
            }

            // Bye messages
            if (!process.env.LOCAL && guild.id === SORBONNE_JUSSIEU_SNOWFLAKE) {
                const byeMessage = await getRandomByeMessage();
                const bye = byeMessage.message.replace(
                    '{name}',
                    member.displayName
                );

                const byeChannel = (await client.channels.fetch(
                    BYE_CHANNEL_SNOWFLAKE
                )) as TextChannel;

                await byeChannel.send(bye);
            }
        } catch (error) {
            dislog(
                'wtf',
                singleEmbedMessage(
                    internalErrorEmbed('`guildMemberRemove` event', error)
                )
            );
        }
    }
);

client.on('guildCreate', async (guild: Guild) => {
    await createSuGuild(guild);

    dislog(
        'moderation',
        singleEmbedMessage(
            infoEmbed()
                .setTitle(`Guild ${formatGuild(guild)} integrated`)
                .setDescription(`A new guild was added to the database.`)
        )
    );
});

client.login(process.env.SORBOT_TOKEN);
