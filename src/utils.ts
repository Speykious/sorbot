import { readFileSync, writeFileSync } from 'fs';
import { resolve } from 'path';

/** Error instance used when an env variable is missing. */
export class MissingEnvError extends Error {
    constructor(envVariable: string) {
        super(`Env variable ${envVariable} is missing!`);
    }
}

/** Constructs a path relative to the working directory. */
export function relative(path: string): string {
    return resolve(__dirname, '../', path);
}

/** Reads a file using a path relative to the working directory. */
export function readf(path: string): string {
    return readFileSync(relative(path), 'utf8');
}

/** Writes a file to a path relative to the working directory. */
export function writef(path: string, data: string | NodeJS.ArrayBufferView) {
    return writeFileSync(relative(path), data, 'utf8');
}

/** Asserts that all rows of the table have the same dimension. */
function assertConsistentRowDimensions(headers: unknown[], table: unknown[][]) {
    const width = headers.length;
    for (const row of table) {
        if (width !== row.length)
            throw new Error(
                `Cannot write markdown table: inconsistent row widths (${row.length} != ${width})`
            );
    }
}

/** Creates a markdown table from a matrix of strings. */
export function markdownTable(headers: string[], table: string[][]): string {
    // No for loops in this function. Functional programming ftw :D

    if (table.length === 0) return '';
    assertConsistentRowDimensions(headers, table);

    const columnWidths: number[] = headers.map((header, index) =>
        Math.max(3, header.length, ...table.map(row => row[index].length))
    );

    const mdHeaders: string[] = headers.map((header, index) =>
        header.padEnd(columnWidths[index])
    );
    const mdSeparators: string[] = mdHeaders.map(header =>
        '-'.repeat(header.length)
    );
    const mdRows: string[][] = table.map(row =>
        row.map((value, index) => value.padEnd(columnWidths[index]))
    );

    return [mdHeaders, mdSeparators, ...mdRows]
        .map(row => `| ${row.join(' | ')} |`)
        .join('\n');
}
