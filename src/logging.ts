import {
    Client,
    Guild,
    Message,
    MessageCreateOptions,
    MessagePayload,
    TextChannel,
    User,
} from 'discord.js';
import { LogChannelName, LogsCache, LogSnowflakes } from './interfaces';

/** Snowflakes of all log channels. */
export const logSnowflakes: LogSnowflakes = {
    guild: '688752122388414587',
    localChannels: {
        init: '755788441974997073',
        email: '755789937693163601',
        database: '755789949055402144',
        messages: '755789986430845009',
        moderation: '755789998845984841',
        wtf: '755790008329175070',
        warning: '755792042302636152',
        interactions: '976186351890333746',
    },
    channels: {
        init: '755792762112180324',
        email: '755792774359679037',
        database: '755792786153930773',
        messages: '755792800892846121',
        moderation: '755792819041468547',
        wtf: '755792840277098536',
        warning: '755147975059308726',
        interactions: '976186330843324436',
    },
};

const channelMapName = process.env.LOCAL ? 'localChannels' : 'channels';

/** Cache of all used log channels. */
const logsCache: LogsCache = {
    client: undefined,
    channels: {
        init: undefined,
        email: undefined,
        database: undefined,
        messages: undefined,
        moderation: undefined,
        wtf: undefined,
        warning: undefined,
        interactions: undefined,
    },
};

/** Initializes the log cache. */
export function initLogs(client: Client) {
    // Yup, that's all we need to do here
    logsCache.client = client;
}

/** Logs a message to a log Discord channel. */
export async function dislog(
    logChannelName: LogChannelName,
    content: string | MessagePayload | MessageCreateOptions
): Promise<Message<boolean> | undefined> {
    const { client } = logsCache;

    if (!client) {
        console.error('Warning: trying to log while client is not ready');
        return;
    }

    // Cache the logging channel
    if (!logsCache.channels[logChannelName]) {
        logsCache.channels[logChannelName] = (await client.channels.fetch(
            logSnowflakes[channelMapName][logChannelName]
        )) as TextChannel;
    }

    return await logsCache.channels[logChannelName]?.send(content);
}

/** Represents a Discord user in a pretty way for dislogs. */
export function formatUser(user: User): string {
    return `**${user.tag}** (${user.id})`;
}

/** Represents a Discord guild in a pretty way for dislogs. */
export function formatGuild(guild: Guild): string {
    return `**__${guild.name}__** (${guild.id})`;
}
