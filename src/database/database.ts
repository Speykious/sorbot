import { Sequelize } from 'sequelize';
import { genSuUser } from './models/SuUser';
import { genSuGuild } from './models/SuGuild';
import { genByeMessage } from './models/ByeMessage';
import { genEmailDomain } from './models/EmailDomain';

const { USER, DB_USER, DB_PASS, DB_SOCKET_PATH, LOCAL } = process.env;

const sequelize = new Sequelize({
    database: LOCAL ? 'sorbot' : (USER ?? 'sorbot'),
    username: DB_USER,
    password: DB_PASS,
    dialect: 'postgres',
    host: LOCAL ? 'localhost' : (DB_SOCKET_PATH || '/tmp'),
    dialectModule: require('pg'),
    logging: false, // literally yeet loggings into oblivion
});

genSuGuild(sequelize);
genSuUser(sequelize);
genByeMessage(sequelize);
genEmailDomain(sequelize);

sequelize
    .sync()
    .then(() => console.log('Database connection successfully based'))
    .catch(error => {
        console.error('Error while trying to connect to the database:', error);
        process.exit(727); // wysi
    });

export default sequelize;
