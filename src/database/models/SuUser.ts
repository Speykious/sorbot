import { Snowflake, User } from 'discord.js';
import { ARRAY, BOOLEAN, Model, Sequelize, STRING } from 'sequelize';
import { CipheredString, encrypt, HashedString } from '../../encryption';
import { dislog, formatUser } from '../../logging';
import SuGuild from './SuGuild';

/**
 * `SuUser` model. It stores data about a Sorbonne user in our database,
 * notably their ciphered Discord ID and hashed email address, which of
 * our servers they're in, and networkwide role tags.
 *
 * It also saves state during their verification process, such as the confirmation code.
 */
export default class SuUser extends Model {
    /** Snowflake of the user. */
    declare id: CipheredString;
    /**
     * Email address of the user. It is ciphered while the user is going through
     * the verification process, and is hashed when they become verified.
     */
    declare email: CipheredString | HashedString | null;
    /** Confirmation code. It is only used during the verification process, null otherwise. */
    declare code: string | null;
    /** Whether the user is verified. */
    declare verified: boolean;
    /** Role tags. They let Sorbot synchronize roles between federated servers. */
    declare roletags: Set<string>;
    /** Which of our servers this user is in. */
    declare servers: Set<Snowflake>;

    /** Add a role tag to this user. */
    addRoletag(roletag: string): boolean {
        const roletags = this.roletags;
        const added = !roletags.has(roletag);
        roletags.add(roletag);
        this.roletags = roletags;
        return added;
    }

    /** Remove a role tag from this user. */
    removeRoletag(roletag: string): boolean {
        const roletags = this.roletags;
        const removed = roletags.delete(roletag);
        this.roletags = roletags;
        return removed;
    }

    /** Add a server this user is in. */
    addServer(server: string): boolean {
        const servers = this.servers;
        const added = !servers.has(server);
        servers.add(server);
        this.servers = servers;
        return added;
    }

    /** Remove a server this user was in. */
    removeServer(server: string): boolean {
        const servers = this.servers;
        const removed = servers.delete(server);
        this.servers = servers;
        return removed;
    }
}

/**
 * Get a `SuUser` from a Discord `User` if it exists.
 * @param user The Discord user.
 * @param logError Whether to log when it cannot find the user in the database.
 * @returns The corresponding `SuUser` instance.
 */
export async function getSuUser(
    user: User,
    logError = true
): Promise<SuUser | null> {
    const suUser = await SuUser.findByPk(encrypt(user.id));
    if (!suUser && logError) {
        dislog(
            'database',
            `User ${formatUser(user)} does not exist in our database`
        );
    }
    return suUser;
}

/**
 * Get a `SuUser` from a Discord `User` if it exists, creates it otherwise.
 * @param user The Discord user.
 * @returns The corresponding `SuUser` instance.
 */
export async function touchSuUser(user: User): Promise<SuUser> {
    let suUser = await getSuUser(user, false);
    if (!suUser) {
        const servers: Snowflake[] = [];
        const suGuilds = await SuGuild.findAll();

        await Promise.all(
            suGuilds.map(async suGuild => {
                const guild = await user.client.guilds.fetch(suGuild.id);
                try {
                    await guild.members.fetch(user);
                    servers.push(guild.id);
                } catch (error) {
                    // Member doesn't exist
                }
            })
        );

        suUser = await SuUser.create({
            id: user.id,
            verified: false,
            servers,
        });
    }
    return suUser;
}

/** Initializes the `SuUser` model. */
export function genSuUser(sequelize: Sequelize): typeof SuUser {
    return SuUser.init(
        {
            id: {
                type: STRING,
                primaryKey: true,
                set: function (value: string) {
                    const di = encrypt(value);
                    this.setDataValue('id', di);
                },
            },
            email: {
                type: STRING,
                unique: true,
            },
            code: {
                type: STRING(6),
            },
            verified: {
                type: BOOLEAN,
                allowNull: false,
            },
            roletags: {
                type: ARRAY(STRING),
                defaultValue: [],
                allowNull: false,
                get: function (): Set<string> {
                    return new Set(this.getDataValue('roletags'));
                },
                set: function (value: Set<string>) {
                    this.setDataValue('roletags', [...value.values()]);
                },
            },
            servers: {
                type: ARRAY(STRING),
                allowNull: false,
                get: function (): Set<string> {
                    return new Set(this.getDataValue('servers'));
                },
                set: function (value: Set<string>) {
                    this.setDataValue('servers', [...value.values()]);
                },
            },
        },
        {
            sequelize,
            modelName: SuUser.name,
        }
    );
}
