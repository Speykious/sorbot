import { Model, Sequelize, STRING } from 'sequelize';

export type EmailDomainType = 'student' | 'professor';

/**
 * `EmailDomain` model. It stores email domains in our database.
 * These domains are considered valid Sorbonne domains and let a
 * user verify themselves with an email address of that domain.
 */
export default class EmailDomain extends Model {
    /** A valid Sorbonne email domain. */
    declare domain: string;
    /** Which type of user uses such a domain. */
    declare domainType: EmailDomainType;
}

/**
 * Get all valid email domains of a domain type.
 *
 * **NOTE:** do not use this function to check if an email domain is valid.
 * @param domainType The domain type.
 * @returns All valid email domains of this domain type.
 */
export async function getEmailDomains(
    domainType: EmailDomainType
): Promise<string[]> {
    const emailDomains = await EmailDomain.findAll({ where: { domainType } });
    return emailDomains.map(({ domain }) => domain);
}

/** Initializes the `EmailDomain` model. */
export function genEmailDomain(sequelize: Sequelize): typeof EmailDomain {
    return EmailDomain.init(
        {
            domain: {
                type: STRING,
                primaryKey: true,
                allowNull: false,
            },
            domainType: {
                type: STRING,
                values: ['student', 'professor'],
                allowNull: false,
            },
        },
        {
            sequelize,
            modelName: EmailDomain.name,
        }
    );
}
