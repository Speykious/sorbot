import { Snowflake } from 'discord.js';
import { Model, Sequelize, STRING } from 'sequelize';
import sequelize from '../database';

/** `ByeMessage` model. It stores bye messages in our database. */
export default class ByeMessage extends Model {
    /** Snowflake of the message this `ByeMessage` originated from. */
    declare id: Snowflake;
    /** Author of the `ByeMessage`. */
    declare author: Snowflake;
    /** Content of the `ByeMessage`. */
    declare message: string;
}

/** Selects a random `ByeMessage` and returns it. */
export async function getRandomByeMessage(): Promise<ByeMessage> {
    const byeMessage = await ByeMessage.findOne({ order: sequelize.random() });
    if (!byeMessage) throw new Error('There are no bye messages');
    return byeMessage;
}

/** Initializes the `ByeMessage` model. */
export function genByeMessage(sequelize: Sequelize): typeof ByeMessage {
    return ByeMessage.init(
        {
            id: {
                type: STRING,
                primaryKey: true,
                allowNull: false,
            },
            author: {
                type: STRING,
            },
            message: {
                type: STRING,
            },
        },
        {
            sequelize,
            modelName: ByeMessage.name,
        }
    );
}
