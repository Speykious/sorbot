import { Guild, Snowflake } from 'discord.js';
import { Model, Sequelize, STRING } from 'sequelize';
import { StringAssociations } from '../../interfaces';
import { dislog, formatGuild } from '../../logging';

/**
 * `SuGuild` model. It stores data about a federated server in our database,
 * notably role associations which let Sorbot synchronize common roles between servers.
 */
export default class SuGuild extends Model {
    /** Snowflake of the guild. */
    declare id: Snowflake;
    /** Name of the guild. */
    declare name: string;
    /** Default channel's snowflake. */
    declare defaultChannel: Snowflake | null;
    /** Role associations. They let Sorbot synchronize common roles between servers. */
    declare roleassocs: StringAssociations;

    /** Add a role association to this `SuGuild`. */
    addRoleassoc(name: string, role: string) {
        this.roleassocs = {
            ...this.roleassocs,
            [name]: role,
        };
    }

    /** Remove a role association from this `SuGuild`. */
    removeRoleassoc(name: string) {
        const roleassocs = this.roleassocs;
        delete roleassocs[name];
        this.roleassocs = roleassocs;
    }

    /** Markdown link to the guild's default channel. */
    get mdLink(): string {
        return this.defaultChannel
            ? `[${this.name}](https://discord.com/channels/${this.id}/${this.defaultChannel})`
            : this.name;
    }
}

/**
 * Get an `SuGuild` from a Discord `Guild`.
 * @param guild The Discord guild.
 * @param logError Whether to log when it cannot find the user in the database.
 * @returns The corresponding `SuGuild` instance.
 */
export async function getSuGuild(
    guild: Guild,
    logError = true
): Promise<SuGuild | null> {
    const suGuild = await SuGuild.findByPk(guild.id);
    if (!suGuild && logError)
        dislog(
            'database',
            `Guild ${formatGuild(guild)} does not exist in our database`
        );
    return suGuild;
}

/**
 * Create an `SuGuild` from a Discord `Guild`.
 * @param guild The Discord guild.
 * @returns The corresponding `SuGUild` instance.
 */
export async function createSuGuild(
    guild: Guild,
    logWarn = true
): Promise<SuGuild> {
    const suGuild = await SuGuild.findByPk(guild.id);
    if (suGuild) {
        if (logWarn)
            dislog(
                'database',
                `Guild ${formatGuild(guild)} already exists in our database`
            );
        return suGuild;
    }

    return await SuGuild.create({
        id: guild.id,
        name: guild.name,
        defaultChannel: guild.systemChannelId,
    });
}

/** Initializes the `SuGuild` model. */
export function genSuGuild(sequelize: Sequelize): typeof SuGuild {
    return SuGuild.init(
        {
            id: {
                type: STRING,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: STRING,
                allowNull: false,
            },
            defaultChannel: {
                type: STRING,
                allowNull: true,
            },
            roleassocs: {
                type: STRING(512),
                get: function (): StringAssociations {
                    const roleassocs: string | null =
                        this.getDataValue('roleassocs');
                    if (!roleassocs || roleassocs === '') return {};
                    const entries = roleassocs
                        .split('\n')
                        .map(line => line.split(':') as [string, string]);
                    return Object.fromEntries(entries);
                },
                set: function (value: StringAssociations) {
                    this.setDataValue(
                        'roleassocs',
                        Object.entries(value)
                            .map(assoc => assoc.join(':'))
                            .join('\n')
                    );
                },
                allowNull: true,
            },
        },
        {
            sequelize,
            modelName: SuGuild.name,
        }
    );
}
