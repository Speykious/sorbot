import { hash } from 'argon2';
import {
    CommandInteraction,
    MessageComponentInteraction,
    ModalSubmitInteraction,
    User,
} from 'discord.js';
import SuUser, { getSuUser, touchSuUser } from '../database/models/SuUser';
import { getSorbonneDomain } from '../email/validation';
import { decrypt } from '../encryption';
import { syncRoles } from '../roles';

/**
 * Verify the user.
 * This also attributes role tags
 */
export async function verifyUser(user: User, suUser: SuUser) {
    const { email } = suUser;
    const roletags = ['verified'];

    if (email) {
        // Add email domain type to roletags
        const emailDomain = await getSorbonneDomain(decrypt(email));

        // This error should never happen since we fetch it directly from the database.
        // But juuuuuuuuuust in case...
        if (!emailDomain) throw new Error('Email domain is invalid');

        roletags.push(emailDomain.domainType);
    }

    await suUser.update({
        code: null,
        email: email ? await hash(decrypt(email)) : null,
        verified: true,
        roletags: new Set(roletags),
    });

    await syncRoles(user);
}

export type ReplyableInteraction =
    | CommandInteraction
    | ModalSubmitInteraction
    | MessageComponentInteraction;

export async function ephemeralReplyOrEditReply(
    interaction: ReplyableInteraction,
    content: string,
    editReply: boolean
): Promise<void> {
    if (editReply) await interaction.editReply(content);
    else await interaction.reply({ ephemeral: true, content });
}

/** Options specific to `getUnverifiedUserFromInteraction()`. */
export interface GUUFIOptions {
    /** Whether to add the user to the database if it doesn't exist. */
    touchUser?: boolean;
    /** Whether to edit the reply in case `interaction.deferReply()` was called. */
    editReply?: boolean;
    /** Whether the user can be already verified. */
    canBeVerified?: boolean;
}

/**
 * Makes sure the user is unverified.
 * - If they are unverified, the corresponding `SuUser` is returned.
 * - If they are verified, it replies to the interaction accordingly.
 * - If they are no in the database, they are added in if `touchUser` is `true`,
 *   otherwise it replies to the interaction accordingly.
 *
 * **Important note:** if you call this function after `interaction.deferReply()`,
 * make sure to add the option `editReply: true` so that it doesn't crash.
 *
 * @param interaction The interaction created by the Discord user.
 * @returns an unverified `SuUser` or `null`.
 */
export async function getSuUserFromInteraction(
    interaction: ReplyableInteraction,
    options: GUUFIOptions = {}
): Promise<SuUser | null> {
    const touchUser = options.touchUser ?? false;
    const editReply = options.editReply ?? false;
    const canBeVerified = options.canBeVerified ?? false;
    const { user } = interaction;

    let suUser: SuUser | null;
    if (touchUser) {
        suUser = await touchSuUser(user);
    } else {
        suUser = await getSuUser(user, false);

        if (!suUser) {
            const content =
                "Vous n'êtes pas enregistré.e dans notre base de données.";
            await ephemeralReplyOrEditReply(interaction, content, editReply);
            return null;
        }
    }

    if (!canBeVerified && suUser.verified) {
        const content = 'Vous êtes déjà vérifié.e 👍';
        await ephemeralReplyOrEditReply(interaction, content, editReply);
        return null;
    }

    return suUser;
}
