import { ApplicationCommandDataResolvable, Interaction } from 'discord.js';
import { CommandSubhandlers } from '../interfaces';

import roleassoc from './commands/roleassoc';
import no from './commands/no';
import byemsg from './commands/byemsg';
import verify from './commands/verify';
import myservers from './commands/myservers';
import mydata from './commands/mydata';
import suguild from './commands/suguild';
import yeet from './commands/yeet';
import shut from './commands/shut';
import verificator from './commands/verificator';
import sauce from './commands/sauce';

import { dislog, formatUser } from '../logging';
import {
    criticalErrorMessage,
    internalErrorEmbed,
    singleEmbedMessage,
} from '../embeds';

/** All slash commands */
export const commands: CommandSubhandlers = {
    sauce,
    roleassoc,
    no,
    byemsg,
    verify,
    verificator,
    myservers,
    mydata,
    suguild,
    yeet,
    shut,
};

/** Get commands as an array of JSONs. */
export function getCommandData(): ApplicationCommandDataResolvable[] {
    const cmds = [];
    for (const cmd of Object.values(commands))
        if (cmd) cmds.push(cmd.data.toJSON());

    return cmds;
}

/** Command interaction handler */
export async function onCommandInteraction(interaction: Interaction) {
    if (!interaction.isChatInputCommand()) return;

    const { commandName, user } = interaction;
    const command = commands[commandName];
    if (!command) {
        await interaction.reply({
            content: 'Unknown command. :(',
            ephemeral: true,
        });
        return;
    }

    dislog(
        'interactions',
        `${formatUser(user)} used command \`/${commandName}\``
    );

    try {
        await command.run(interaction);
    } catch (error) {
        dislog(
            'wtf',
            singleEmbedMessage(
                internalErrorEmbed(
                    `Command interaction (\`/${commandName}\`)`,
                    error
                )
            )
        );

        if (!(interaction.deferred || interaction.replied))
            await interaction.deferReply({ ephemeral: true });
        await interaction.editReply(criticalErrorMessage('ERREUR DE COMMANDE'));
    }
}
