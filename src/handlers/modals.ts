import { Interaction } from 'discord.js';
import {
    criticalErrorMessage,
    internalErrorEmbed,
    singleEmbedMessage,
} from '../embeds';
import { ModalSubhandlers } from '../interfaces';
import { dislog, formatUser } from '../logging';
import emailAddressForm from './modals/emailAddressForm';
import enterCodeForm from './modals/enterCodeForm';

/** All components */
export const modals: ModalSubhandlers = {
    emailAddressForm,
    enterCodeForm,
};

/** Component interaction handler */
export async function onModalSubmit(interaction: Interaction) {
    if (!interaction.isModalSubmit()) return;

    const { customId, user } = interaction;
    const modal = modals[customId];
    if (!modal) throw new Error(`Unknown modal \`${customId}\``);

    dislog(
        'interactions',
        `${formatUser(user)} submitted modal \`${customId}\``
    );

    try {
        await modal.run(interaction);
    } catch (error) {
        dislog(
            'wtf',
            singleEmbedMessage(internalErrorEmbed('Modal interaction', error))
        );

        if (!(interaction.deferred || interaction.replied))
            await interaction.deferReply({ ephemeral: true });
        await interaction.editReply(criticalErrorMessage('ERREUR DE MODAL'));
    }
}
