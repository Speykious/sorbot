import { Interaction } from 'discord.js';
import {
    criticalErrorMessage,
    internalErrorEmbed,
    singleEmbedMessage,
} from '../embeds';
import { ComponentSubhandlers } from '../interfaces';
import { dislog, formatUser } from '../logging';
import enterCode from './components/enterCode';
import resendCode from './components/resendCode';
import cancel from './components/cancel';
import verifyBtn from './components/verifyBtn';
import validDomains from './components/validDomains';

/** All components */
export const components: ComponentSubhandlers = {
    enterCode,
    resendCode,
    cancel,
    verifyBtn,
    validDomains,
};

/** Component interaction handler */
export async function onComponentInteraction(interaction: Interaction) {
    if (!interaction.isMessageComponent()) return;

    const { customId, user } = interaction;
    const component = components[customId];
    if (!component) throw new Error(`Unknown component \`${customId}\``);

    dislog(
        'interactions',
        `${formatUser(user)} used component \`${customId}\``
    );

    try {
        await component.run(interaction);
    } catch (error) {
        dislog(
            'wtf',
            singleEmbedMessage(
                internalErrorEmbed('Component interaction', error)
            )
        );

        if (!(interaction.deferred || interaction.replied))
            await interaction.deferReply({ ephemeral: true });
        await interaction.editReply(
            criticalErrorMessage('ERREUR DE COMPONENT')
        );
    }
}
