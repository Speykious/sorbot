import {
    ButtonBuilder,
    ButtonStyle,
    MessageComponentInteraction,
} from 'discord.js';
import { getEmailDomains } from '../../database/models/EmailDomain';
import { infoEmbed, singleEmbedMessage } from '../../embeds';
import { ComponentSubhandler } from '../../interfaces';

/** `validDomains` button interaction metadata. */
const data = new ButtonBuilder()
    .setCustomId('validDomains')
    .setEmoji('📧')
    .setLabel('Noms de domaine valides')
    .setStyle(ButtonStyle.Secondary);

/** `validDomains` button interaction handler. */
async function run(interaction: MessageComponentInteraction) {
    await interaction.deferReply({ ephemeral: true });

    const studentDomains = await getEmailDomains('student');
    const professorDomains = await getEmailDomains('professor');

    await interaction.editReply(
        singleEmbedMessage(
            infoEmbed()
                .setTitle('Noms de domaine valides')
                .setDescription(
                    'Seuls ces noms de domaine seront acceptés pour la vérification.'
                )
                .addFields([
                    {
                        name: 'Domaines étudiants',
                        value: studentDomains.join('\n'),
                        inline: true,
                    },
                    {
                        name: 'Domeines enseignants',
                        value: professorDomains.join('\n'),
                        inline: true,
                    },
                ])
        )
    );
}

/** Button to get all valid domains. */
const validDomains: ComponentSubhandler = { data, run };
export default validDomains;
