import {
    ButtonBuilder,
    ButtonStyle,
    MessageComponentInteraction,
} from 'discord.js';
import { ComponentSubhandler } from '../../interfaces';
import { getSuUserFromInteraction } from '../helpers';
import enterCodeForm from '../modals/enterCodeForm';

/** `enterCode` button interaction metadata. */
const data = new ButtonBuilder()
    .setCustomId('enterCode')
    .setEmoji('🔐')
    .setLabel('Entrer le code')
    .setStyle(ButtonStyle.Primary);

/** `enterCode` button interaction handler. */
async function run(interaction: MessageComponentInteraction) {
    const suUser = await getSuUserFromInteraction(interaction);
    if (!suUser) return;

    await interaction.showModal(enterCodeForm.data);
}

/** Button to enter the confirmation code during the verification process. */
const enterCode: ComponentSubhandler = { data, run };
export default enterCode;
