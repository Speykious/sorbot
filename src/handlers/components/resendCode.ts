import {
    ButtonBuilder,
    ButtonStyle,
    MessageComponentInteraction,
} from 'discord.js';
import { sendConfirmationEmail } from '../../email/smtp';
import { generateCode } from '../../email/validation';
import { decrypt } from '../../encryption';
import { ComponentSubhandler } from '../../interfaces';
import { getSuUserFromInteraction } from '../helpers';

/** `resendCode` button interaction metadata. */
const data = new ButtonBuilder()
    .setCustomId('resendCode')
    .setEmoji('📨')
    .setLabel('Renvoyer un code')
    .setStyle(ButtonStyle.Secondary);

/** `resendCode` button interaction handler. */
async function run(interaction: MessageComponentInteraction) {
    const { user } = interaction;

    await interaction.deferReply({ ephemeral: true });

    const suUser = await getSuUserFromInteraction(interaction, {
        editReply: true,
    });
    if (!suUser) return;

    if (!suUser.email) {
        await interaction.editReply(
            "Vous n'avez pas renseigné d'adresse mail."
        );
        return;
    }

    const code = generateCode();
    await suUser.update({
        code,
    });

    // The email is ciphered as the user is unverified.
    const email = decrypt(suUser.email);
    await sendConfirmationEmail(email, user, code);

    await interaction.editReply('Un nouveau code a été renvoyé 📨');
}

/** Button to send a new confirmation email in case the previous one wasn't received. */
const resendCode: ComponentSubhandler = { data, run };
export default resendCode;
