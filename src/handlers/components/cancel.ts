import {
    ButtonBuilder,
    ButtonStyle,
    MessageComponentInteraction,
} from 'discord.js';
import { ComponentSubhandler } from '../../interfaces';
import { getSuUserFromInteraction } from '../helpers';

/** `cancel` button interaction metadata. */
const data = new ButtonBuilder()
    .setCustomId('cancel')
    .setEmoji('🏳️')
    .setLabel('Annuler')
    .setStyle(ButtonStyle.Danger);

/** `cancel` button interaction handler. */
async function run(interaction: MessageComponentInteraction) {
    await interaction.deferReply({ ephemeral: true });

    const suUser = await getSuUserFromInteraction(interaction, {
        editReply: true,
    });
    if (!suUser) return;

    // There is no need to store an unverified user
    // Based GDPR compliance B)
    await suUser.destroy();

    await interaction.editReply({
        content: 'Vérification annulée. 🏳️ :(',
    });
}

/** Button to cancel the verification process. */
const cancel: ComponentSubhandler = { data, run };
export default cancel;
