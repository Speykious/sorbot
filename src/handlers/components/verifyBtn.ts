import {
    ButtonBuilder,
    ButtonStyle,
    MessageComponentInteraction,
} from 'discord.js';
import { getSuUser } from '../../database/models/SuUser';
import { ComponentSubhandler } from '../../interfaces';
import emailAddressForm from '../modals/emailAddressForm';

/** `verifyBtn` button interaction metadata. */
const data = new ButtonBuilder()
    .setCustomId('verifyBtn')
    .setEmoji('✅')
    .setLabel('Se vérifier')
    .setStyle(ButtonStyle.Primary);

/** `verifyBtn` button interaction handler. */
async function run(interaction: MessageComponentInteraction) {
    const { user } = interaction;

    // We don't use getUnverifiedUserFromInteraction() here
    // because we don't want to add the user to the DB at this point.
    const suUser = await getSuUser(user);
    if (suUser?.verified) {
        await interaction.reply({
            content: 'Vous êtes déjà vérifié.e 👍',
            ephemeral: true,
        });
        return;
    }

    await interaction.showModal(emailAddressForm.data);
}

/** Button for a user to verify themselves (like the /verify command). */
const verifyBtn: ComponentSubhandler = { data, run };
export default verifyBtn;
