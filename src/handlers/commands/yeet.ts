import {
    ChatInputCommandInteraction,
    PermissionsBitField,
    SlashCommandBooleanOption,
    SlashCommandBuilder,
    SlashCommandUserOption,
} from 'discord.js';
import { getSuUser } from '../../database/models/SuUser';
import { CommandSubhandler } from '../../interfaces';
import { syncRoles } from '../../roles';

/** `/yeet` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('yeet')
    .setDescription('Unverify a user.')
    .setDefaultMemberPermissions(PermissionsBitField.All)
    .setDMPermission(true)
    .addUserOption(
        new SlashCommandUserOption()
            .setName('user')
            .setDescription('The user to unverify')
            .setRequired(true)
    )
    .addBooleanOption(
        new SlashCommandBooleanOption()
            .setName('sync-roles')
            .setDescription(
                "Whether to synchronize the user's roles when unverifying."
            )
            .setRequired(false)
    ) as SlashCommandBuilder;

/** `/yeet` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const user = options.getUser('user');
    if (!user) throw new Error('Cannot find user to yeet');

    const suUser = await getSuUser(user);

    if (!suUser) {
        await interaction.reply({
            content: `This user is not in our database`,
            ephemeral: true,
        });
        return;
    }

    await interaction.deferReply();

    await suUser.update({
        code: null,
        email: null,
        verified: false,
        roletags: new Set(),
    });

    // syncRoles uses the user's SuUser row in the DB to get roletags
    // so we update the SuUser to an unverified state before deleting it
    const doSyncRoles = options.getBoolean('sync-roles', false) ?? true;
    if (doSyncRoles) await syncRoles(user);

    await suUser.destroy();

    await interaction.editReply({
        content: `<@${user.id}> has been **yeeted**.`,
    });
}

/** Command to yeet users. */
const yeet: CommandSubhandler = { data, run };
export default yeet;
