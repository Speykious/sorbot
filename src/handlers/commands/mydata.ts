import {
    ChatInputCommandInteraction,
    PermissionsBitField,
    SlashCommandBuilder,
} from 'discord.js';
import { infoEmbed, singleEmbedMessage } from '../../embeds';
import { CommandSubhandler } from '../../interfaces';
import { getSuUserFromInteraction } from '../helpers';

/** `/mydata` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('mydata')
    .setDescription('Obtenir mes données stockées dans la base de données')
    .setDefaultMemberPermissions(PermissionsBitField.All)
    .setDMPermission(true);

/** `/mydata` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.deferReply({ ephemeral: true });

    const suUser = await getSuUserFromInteraction(interaction, {
        canBeVerified: true,
        editReply: true,
    });
    if (!suUser) return;

    await interaction.editReply(
        singleEmbedMessage(
            infoEmbed()
                .setTitle('Vos données du réseau Discord Sorbonne Jussieu')
                .setDescription(
                    'Voici toutes vos informations stockées dans notre base de données.'
                )
                .addFields({
                    name: 'Données',
                    value:
                        '```yaml' +
                        `\nid: ${suUser.id}` +
                        `\nemail: ${suUser.email}` +
                        `\ncode: ${suUser.code ? '(hidden)' : 'null'}` +
                        `\nverified: ${suUser.verified}` +
                        `\nroletags: [${[...suUser.roletags.values()]}]` +
                        `\nservers: [${[...suUser.servers.values()]}]` +
                        '\n```',
                })
        )
    );
}

/** Command which lets users mydata themselves. */
const mydata: CommandSubhandler = { data, run };
export default mydata;
