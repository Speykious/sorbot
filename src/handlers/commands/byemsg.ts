import {
    ChatInputCommandInteraction,
    CommandInteraction,
    SlashCommandBuilder,
    SlashCommandStringOption,
    SlashCommandSubcommandBuilder,
} from 'discord.js';
import {
    internalErrorEmbed,
    singleEmbedMessage,
    simpleSuccessEmbed,
    errorEmbed,
} from '../../embeds';
import { CommandSubhandler } from '../../interfaces';
import ByeMessage from '../../database/models/ByeMessage';
import { markdownTable } from '../../utils';

/** `/byemsg` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('byemsg')
    .setDescription('Manage bye messages')
    .setDefaultMemberPermissions(0)
    .setDMPermission(false)
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('list')
            .setDescription('List bye messages')
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('add')
            .setDescription('Add a bye message')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('content')
                    .setDescription(
                        "Content of the bye message (`{name}` will be replaced by the leaving member's username)"
                    )
                    .setRequired(true)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('accept')
            .setDescription('Accept a bye message suggestion')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('message')
                    .setDescription(
                        'ID of the message containing a bye message suggestion'
                    )
                    .setRequired(true)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('remove')
            .setDescription('Remove a bye message')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('snowflake')
                    .setDescription('Index of the bye message')
                    .setRequired(true)
            )
    );

/**
 * `/byemsg list` subcommand handler.
 *
 * Sends a file containing a Markdown table of all bye messages.
 */
async function byemsgList(interaction: CommandInteraction) {
    const byeMessages = await ByeMessage.findAll();

    // These variables are typed this way to ensure I don't mess up the table dimensions.
    const headers: [string, string, string] = ['ID', 'Author', 'Message'];
    const table: [string, string, string][] = byeMessages.map(
        ({ id, author, message }) => [id, author, message]
    );

    await interaction.editReply({
        files: [
            {
                name: 'bye-messages.md',
                description: 'File containing all bye messages.',
                attachment: Buffer.from(markdownTable(headers, table)),
            },
        ],
    });
}

/**
 * `/byemsg add` subcommand handler.
 *
 * Adds a bye message.
 */
async function byemsgAdd(interaction: ChatInputCommandInteraction) {
    const { options, user } = interaction;

    const content = options.getString('content', true);

    await ByeMessage.create({
        id: interaction.id,
        author: user.id,
        message: content,
    });

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed('Added one bye message').addFields([
                { name: 'Author', value: `<@${user.id}>` },
                { name: 'Content', value: content },
            ])
        )
    );
}

/**
 * `/byemsg accept` subcommand handler.
 *
 * Creates and saves a bye message from a Discord message.
 * The author of the bye message is the author of the Discord message.
 *
 * This allows other people to be credited for their suggestions,
 * as well as to keep a history of when a bye message was added in.
 */
async function byemsgAccept(interaction: ChatInputCommandInteraction) {
    const { options, channel } = interaction;

    if (!channel) {
        await interaction.editReply('You have to be in a text channel!');
        return;
    }

    const messageId = options.getString('message', true);
    const message = await channel.messages.fetch(messageId);

    // Safety again character limit in the DB.
    if (message.content.length > 255) {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Message too long!')
                    .setDescription(
                        `The message is ${message.content.length} characters long, but the character limit is 255.`
                    )
            )
        );
        return;
    }

    await ByeMessage.create({
        id: message.id,
        author: message.author.id,
        message: message.content,
    });

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed(
                'Added one bye message from suggestion'
            ).addFields([
                { name: 'Author', value: `<@${message.author.id}>` },
                { name: 'Content', value: message.content },
            ])
        )
    );
}

/**
 * `/byemsg remove` subcommand handler.
 *
 * Removes a bye message from the database.
 */
async function byemsgRemove(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const snowflake = options.getString('snowflake', true);
    const bye = await ByeMessage.findByPk(snowflake);

    if (!bye) {
        await interaction.editReply(
            singleEmbedMessage(
                internalErrorEmbed(
                    'Existential Crisis',
                    `There is no bye message with this snowflake (\`${snowflake}\`)`
                )
            )
        );
        return;
    }

    const { author, message } = bye;
    await bye.destroy();

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed(
                `Destroyed bye message with snowflake \`${snowflake}\``
            ).addFields([
                { name: 'Author', value: `<@${author}>` },
                { name: 'Content', value: message },
            ])
        )
    );
}

/** `/byemsg` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.deferReply();

    const subcommand = interaction.options.getSubcommand();
    switch (subcommand) {
        case 'add':
            await byemsgAdd(interaction);
            break;
        case 'accept':
            await byemsgAccept(interaction);
            break;
        case 'remove':
            await byemsgRemove(interaction);
            break;
        case 'list':
            await byemsgList(interaction);
            break;
        default:
            await interaction.editReply(
                singleEmbedMessage(
                    internalErrorEmbed(
                        'Existential Crisis',
                        `Unknown subcommand \`${subcommand}\``
                    )
                )
            );
            break;
    }
}

/** Set of subcommands to manage bye messages. */
const byemsg: CommandSubhandler = { data, run };
export default byemsg;
