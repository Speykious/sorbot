import {
    ChatInputCommandInteraction,
    Role,
    SlashCommandBuilder,
    SlashCommandRoleOption,
    SlashCommandStringOption,
    SlashCommandSubcommandBuilder,
} from 'discord.js';
import {
    internalErrorEmbed,
    infoEmbed,
    singleEmbedMessage,
    simpleSuccessEmbed,
} from '../../embeds';
import SuGuild from '../../database/models/SuGuild';
import { CommandSubhandler } from '../../interfaces';

/** `/roleassoc` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('roleassoc')
    .setDescription('Manage role associations for a guild')
    .setDefaultMemberPermissions(0)
    .setDMPermission(false)
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('list')
            .setDescription('List role associations')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('guild')
                    .setDescription('ID of the guild')
                    .setRequired(false)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('add')
            .setDescription('Add a role association')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('name')
                    .setDescription(
                        'Name of the role association, which is common among federated servers'
                    )
                    .setRequired(true)
            )
            .addRoleOption(
                new SlashCommandRoleOption()
                    .setName('role')
                    .setDescription('Role to associate')
                    .setRequired(true)
            )
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('guild')
                    .setDescription('ID of the guild')
                    .setRequired(false)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('remove')
            .setDescription('Remove a role association')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('name')
                    .setDescription(
                        'Name of the role association, which is common among federated servers'
                    )
                    .setRequired(true)
            )
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('guild')
                    .setDescription('ID of the guild')
                    .setRequired(false)
            )
    );

/**
 * `/roleassoc list` subcommand handler.
 *
 * Sends an embed listing all role associations of this guild.
 */
async function roleassocList(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const guildId = options.getString('guild') ?? interaction.guildId;
    if (!guildId) {
        await interaction.editReply(
            'You have to be in a guild or specify one!'
        );
        return;
    }

    const suGuild = await SuGuild.findByPk(guildId);
    if (!suGuild) {
        await interaction.editReply(
            singleEmbedMessage(
                internalErrorEmbed(
                    'Existential Crisis',
                    `Guild with ID \`${guildId}\` does not exist in our database!`
                )
            )
        );
        return;
    }

    const guild = await interaction.client.guilds.fetch(guildId);

    const roleassocEntries = Object.entries(suGuild.roleassocs);
    const roleassocNames = roleassocEntries.map(([name]) => name);
    const roleassocRoles = await Promise.all(
        roleassocEntries.map(async ([, id]) => {
            const role: Role | null = await guild.roles.fetch(id);
            return `@${role?.name}`;
        })
    );

    const namesStr = roleassocNames.join('\n');
    const rolesStr = roleassocRoles.join('\n');

    await interaction.editReply(
        singleEmbedMessage(
            infoEmbed()
                .setTitle('Role associations')
                .setDescription(
                    `List of role associations in guild ${suGuild.mdLink}`
                )
                .addFields([
                    {
                        name: 'Name',
                        value: namesStr === '' ? '(empty)' : namesStr,
                        inline: true,
                    },
                    {
                        name: 'Role',
                        value: rolesStr === '' ? '(emptiness)' : rolesStr,
                        inline: true,
                    },
                ])
        )
    );
}

/**
 * `/roleassoc add` subcommand handler.
 *
 * Adds a role association to this guild.
 */
async function roleassocAdd(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const guildId = options.getString('guild') ?? interaction.guildId;
    if (!guildId) {
        await interaction.editReply(
            'You have to be in a guild or specify one!'
        );
        return;
    }

    const suGuild = await SuGuild.findByPk(guildId);
    if (!suGuild) {
        await interaction.editReply(
            singleEmbedMessage(
                internalErrorEmbed(
                    'Existential Crisis',
                    `Guild with ID \`${guildId}\` does not exist in our database!`
                )
            )
        );
        return;
    }

    const name = options.getString('name', true);
    const role = options.getRole('role', true);
    suGuild.addRoleassoc(name, role.id);
    suGuild.save();

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed(
                `Associated role **@${role.name}** as \`${name}\` in guild ${suGuild.mdLink}`
            )
        )
    );
}

/**
 * `/roleassoc remove` subcommand handler.
 *
 * Removes a role association from this guild.
 */
async function roleassocRemove(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const guildId = options.getString('guild') ?? interaction.guildId;
    if (!guildId) {
        await interaction.editReply(
            'You have to be in a guild or specify one!'
        );
        return;
    }

    const suGuild = await SuGuild.findByPk(guildId);
    if (!suGuild) {
        await interaction.editReply(
            singleEmbedMessage(
                internalErrorEmbed(
                    'Existential Crisis',
                    `Guild with ID \`${guildId}\` does not exist in our database!`
                )
            )
        );
        return;
    }

    const name = options.getString('name', true);
    suGuild.removeRoleassoc(name);
    suGuild.save();

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed(
                `Removed role association \`${name}\` in guild ${suGuild.mdLink}`
            )
        )
    );
}

/** `/roleassoc` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.deferReply({ ephemeral: true });

    const subcommand = interaction.options.getSubcommand();
    switch (subcommand) {
        case 'add':
            await roleassocAdd(interaction);
            break;
        case 'remove':
            await roleassocRemove(interaction);
            break;
        case 'list':
            await roleassocList(interaction);
            break;
        default:
            await interaction.editReply(
                singleEmbedMessage(
                    internalErrorEmbed(
                        'Existential Crisis',
                        `Unknown subcommand \`${subcommand}\``
                    )
                )
            );
            break;
    }
}

/** Set of subcommands to manage role associations in one of our servers. */
const roleassoc: CommandSubhandler = { data, run };
export default roleassoc;
