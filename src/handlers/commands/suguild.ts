import {
    ChatInputCommandInteraction,
    SlashCommandBuilder,
    SlashCommandChannelOption,
    SlashCommandStringOption,
    SlashCommandSubcommandBuilder,
} from 'discord.js';
import {
    internalErrorEmbed,
    infoEmbed,
    singleEmbedMessage,
    simpleSuccessEmbed,
    warningEmbed,
} from '../../embeds';
import SuGuild, {
    createSuGuild,
    getSuGuild,
} from '../../database/models/SuGuild';
import { CommandSubhandler } from '../../interfaces';
import { dislog, formatGuild } from '../../logging';

/** `/suguild` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('suguild')
    .setDescription('Manage SU guilds')
    .setDefaultMemberPermissions(0)
    .setDMPermission(false)
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('list')
            .setDescription('List SU guilds')
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('add')
            .setDescription('Add an SU guild to the database')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('guild')
                    .setDescription('ID of the guild')
                    .setRequired(false)
            )
            .addChannelOption(
                new SlashCommandChannelOption()
                    .setName('default_channel')
                    .setDescription(
                        'Default channel of the guild (used in markdown links, for example in `/myservers`'
                    )
                    .setRequired(false)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('remove')
            .setDescription('Remove an SU guild to the database')
            .addStringOption(
                new SlashCommandStringOption()
                    .setName('guild')
                    .setDescription('ID of the guild')
                    .setRequired(false)
            )
    )
    .addSubcommand(
        new SlashCommandSubcommandBuilder()
            .setName('leave')
            .setDescription('Get the F outta here')
    );

/**
 * `/suguild list` subcommand handler.
 *
 * Sends an embed listing all SU guilds.
 */
async function suguildList(interaction: ChatInputCommandInteraction) {
    const suGuilds = await SuGuild.findAll();

    const guildNameLinks = suGuilds.map(g => g.mdLink);
    await interaction.editReply(
        singleEmbedMessage(
            infoEmbed()
                .setTitle('All SU Guilds')
                .setDescription('All SU guilds registered into Sorbot.')
                .addFields({
                    name: 'Guilds',
                    value: guildNameLinks.join('\n'),
                    inline: true,
                })
        )
    );
}

/**
 * `/suguild add` subcommand handler.
 *
 * Adds an SU guild to Sorbot.
 */
async function suguildAdd(interaction: ChatInputCommandInteraction) {
    const { options, client } = interaction;

    const guildId = options.getString('guild') ?? interaction.guildId;
    if (!guildId) {
        await interaction.editReply(
            'You have to be in a guild or specify one!'
        );
        return;
    }

    try {
        const guild = await client.guilds.fetch(guildId);
        const suGuild = await createSuGuild(guild);

        const defaultChannel = options.getChannel('default_channel');
        if (defaultChannel) suGuild.defaultChannel = defaultChannel.id;
        suGuild.save();

        await interaction.editReply(
            singleEmbedMessage(
                simpleSuccessEmbed(`Registered SU guild ${suGuild.mdLink}`)
            )
        );
    } catch (e) {
        await interaction.editReply(
            singleEmbedMessage(internalErrorEmbed('Guild fetch crisis', e))
        );
    }
}

/**
 * `/suguild remove` subcommand handler.
 *
 * Removes an SU guild from Sorbot.
 */
async function suguildRemove(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    const guildId = options.getString('guild') ?? interaction.guildId;
    if (!guildId) {
        await interaction.editReply(
            'You have to be in a guild or specify one!'
        );
        return;
    }

    const suGuild = await SuGuild.findByPk(guildId);
    if (!suGuild) {
        await interaction.editReply(
            singleEmbedMessage(
                internalErrorEmbed(
                    'Existential Crisis',
                    `Guild with ID \`${guildId}\` does not exist in our database!`
                )
            )
        );
        return;
    }

    await suGuild.destroy();

    await interaction.editReply(
        singleEmbedMessage(
            simpleSuccessEmbed(`Removed SU guild ${suGuild.mdLink}`)
        )
    );
}

/**
 * `/suguild leave` subcommand handler.
 *
 * Makes Sorbot leave this guild. The corresponding SU guild will be deleted if it exists.
 */
async function suguildLeave(interaction: ChatInputCommandInteraction) {
    const { guild } = interaction;
    if (!guild) {
        await interaction.editReply(
            'You have to be in a guild to make Sorbot leave it!'
        );
        return;
    }

    const suGuild = await getSuGuild(guild);
    suGuild?.destroy();

    await interaction.editReply(
        singleEmbedMessage(
            warningEmbed()
                .setTitle('Ok')
                .setDescription('imma get the F outta here')
                .setImage(
                    'https://external-preview.redd.it/wHkHZV5XeqzQAn-KL0yYtq16dgZ6QA1zv5R5339oe6c.jpg?s=b03a00c8263b4d45c4060a8d24de7913105d2383'
                )
        )
    );

    await guild.leave();
    dislog('moderation', `Sorbot left guild ${formatGuild(guild)}`);
}

/** `/suguild` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.deferReply();

    const subcommand = interaction.options.getSubcommand();
    switch (subcommand) {
        case 'add':
            await suguildAdd(interaction);
            break;
        case 'remove':
            await suguildRemove(interaction);
            break;
        case 'list':
            await suguildList(interaction);
            break;
        case 'leave':
            await suguildLeave(interaction);
            break;
        default:
            await interaction.editReply(
                singleEmbedMessage(
                    internalErrorEmbed(
                        'Existential Crisis',
                        `Unknown subcommand \`${subcommand}\``
                    )
                )
            );
            break;
    }
}

/** Set of subcommands to manage role associations in one of our servers. */
const suguild: CommandSubhandler = { data, run };
export default suguild;
