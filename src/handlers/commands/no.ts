import { SlashCommandBuilder } from 'discord.js';
import { CommandSubhandler } from '../../interfaces';

/** `/no` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('no')
    .setDescription('Literally throw an error.')
    .setDefaultMemberPermissions(0)
    .setDMPermission(false);

/** `/no` command interaction handler. */
async function run() {
    throw new Error('no.');
}

/** no. */
const no: CommandSubhandler = { data, run };
export default no;
