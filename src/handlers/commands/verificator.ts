import {
    ActionRowBuilder,
    ChatInputCommandInteraction,
    MessageActionRowComponentBuilder,
    SlashCommandBuilder,
} from 'discord.js';
import {
    errorEmbed,
    infoEmbed,
    simpleSuccessEmbed,
    singleEmbedMessage,
} from '../../embeds';
import { CommandSubhandler } from '../../interfaces';
import validDomains from '../components/validDomains';
import verifyBtn from '../components/verifyBtn';

/** `/verificator` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('verificator')
    .setDescription('Send a Verify button interaction message in this channel.')
    .setDefaultMemberPermissions(0)
    .setDMPermission(false);

/** `/verificator` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    const { channel } = interaction;

    await interaction.deferReply({ ephemeral: true });

    if (!channel) {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Canot send verificator message')
                    .setDescription(
                        'You have to enter this command in a text channel.'
                    )
            )
        );
        return;
    }

    await channel.send({
        embeds: [
            infoEmbed()
                .setTitle('Vérifiez-vous en tant que Membre !')
                .setDescription(
                    'Vous vérifier en tant que membre donne accès à la partie universitaire du serveur.' +
                        '\nVous devez avoir **une adresse mail valide de Sorbonne Université**.' +
                        '\nPlusieurs noms de domaine sont acceptés outre `@etu.sorbonne-universite.fr`, notamment ceux des laboratoires comme `@lip6.fr`.' +
                        '\nCe nom de domaine permet de déterminer si vous êtes étudiant ou enseignant, et vous attribuera le rôle correspondant.'
                ),
        ],
        components: [
            new ActionRowBuilder<MessageActionRowComponentBuilder>().addComponents(
                verifyBtn.data,
                validDomains.data
            ),
        ],
    });

    await interaction.editReply(
        singleEmbedMessage(simpleSuccessEmbed('Verificator sent!'))
    );
}

/** Command which sends a Verify button interaction message in a given channel. */
const verify: CommandSubhandler = { data, run };
export default verify;
