import {
    ChatInputCommandInteraction,
    PermissionsBitField,
    SlashCommandBuilder,
} from 'discord.js';
import emailAddressForm from '../modals/emailAddressForm';
import { getSuUser } from '../../database/models/SuUser';
import { CommandSubhandler } from '../../interfaces';

/** `/verify` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('verify')
    .setDescription(
        'Se vérifier comme étudiant.e/enseignant.e à Sorbonne Université'
    )
    .setDefaultMemberPermissions(PermissionsBitField.All)
    .setDMPermission(true);

/** `/verify` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    const { user } = interaction;

    // We don't use getUnverifiedUserFromInteraction() here
    // because we don't want to add the user to the DB at this point.
    const suUser = await getSuUser(user);
    if (suUser?.verified) {
        await interaction.reply({
            content: 'Vous êtes déjà vérifié.e 👍',
            ephemeral: true,
        });
        return;
    }

    await interaction.showModal(emailAddressForm.data);
}

/** Command which lets users verify themselves. */
const verify: CommandSubhandler = { data, run };
export default verify;
