import {
    ActionRowBuilder,
    ButtonBuilder,
    ButtonStyle,
    ChatInputCommandInteraction,
    MessageActionRowComponentBuilder,
    PermissionsBitField,
    SlashCommandBuilder,
} from 'discord.js';
import { CommandSubhandler } from '../../interfaces';
import { infoEmbed } from '../../embeds';

/** `/sauce` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('sauce')
    .setDescription("Get the link to Sorbot's source code.")
    .setDefaultMemberPermissions(PermissionsBitField.All)
    .setDMPermission(true);

/** `/sauce` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.reply({
        embeds: [
            infoEmbed()
                .setTitle('Code source de Sorbot')
                .setDescription(
                    'Voici le lien vers le code source de Sorbot.\n' +
                        'En effet, Sorbot est totalement libre et open-source, sous licence GPL v3.'
                ),
        ],
        components: [
            new ActionRowBuilder<MessageActionRowComponentBuilder>().addComponents(
                new ButtonBuilder()
                    .setEmoji('📖')
                    .setLabel('Code source')
                    .setURL('https://gitlab.com/Speykious/sorbot')
                    .setStyle(ButtonStyle.Link)
            ),
        ],
    });
}

const sauce: CommandSubhandler = { data, run };
export default sauce;
