import {
    ChatInputCommandInteraction,
    PermissionsBitField,
    SlashCommandBuilder,
} from 'discord.js';
import { CommandSubhandler } from '../../interfaces';
import { getSuUserFromInteraction } from '../helpers';
import SuGuild from '../../database/models/SuGuild';
import { Sequelize } from 'sequelize';
import { infoEmbed, singleEmbedMessage } from '../../embeds';

/** `/myservers` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('myservers')
    .setDescription('Voir dans quels serveurs fédérés vous vous trouvez')
    .setDefaultMemberPermissions(PermissionsBitField.All)
    .setDMPermission(true);

/** `/myservers` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    await interaction.deferReply({ ephemeral: true });

    const suUser = await getSuUserFromInteraction(interaction, {
        canBeVerified: true,
        editReply: true,
    });
    if (!suUser) return;

    const { servers } = suUser;

    const suGuilds = await SuGuild.findAll({
        where: Sequelize.or(...[...servers.values()].map(s => ({ id: s }))),
    });

    const guildNameLinks = suGuilds.map(g => g.mdLink);
    await interaction.editReply(
        singleEmbedMessage(
            infoEmbed()
                .setTitle('Vos serveurs fédérés')
                .setDescription('Les serveurs fédérés dans lesquels vous êtes.')
                .addFields({
                    name: 'Serveurs',
                    value: guildNameLinks.join('\n'),
                    inline: true,
                })
        )
    );
}

/** Command to list which of our servers the user is in. */
const myservers: CommandSubhandler = { data, run };
export default myservers;
