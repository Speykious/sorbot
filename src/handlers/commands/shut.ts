import {
    ChatInputCommandInteraction,
    SlashCommandBuilder,
    SlashCommandNumberOption,
} from 'discord.js';
import { warningEmbed } from '../../embeds';
import { CommandSubhandler } from '../../interfaces';

/** `/shut` command interaction metadata. */
const data = new SlashCommandBuilder()
    .setName('shut')
    .setDescription('SHUT')
    .addNumberOption(
        new SlashCommandNumberOption()
            .setName('status_code')
            .setDescription('Exit status code of the application')
            .setMinValue(0)
            .setRequired(false)
    )
    .setDefaultMemberPermissions(0)
    .setDMPermission(false);

/**
 * meow~
 *
 * - https://youtu.be/ESx_hy1n7HA
 * - https://youtu.be/JB5gfmWQzSA
 */
const world = {
    /** Execution, Execution, Execution, Execution */
    execute: (me: number) => {
        console.log('Execution~');
        const O_O = me;
        process.exit(O_O);
    },
};

/** `/shut` command interaction handler. */
async function run(interaction: ChatInputCommandInteraction) {
    const { options } = interaction;

    await interaction.reply({
        embeds: [
            warningEmbed()
                .setTitle('SHUT')
                .setDescription('```java\nworld.execute(me);\n```'),
        ],
    });

    const me = options.getNumber('status_code') ?? 0;

    // gottem
    world.execute(me);
}

/** SHUT */
const shut: CommandSubhandler = { data, run };
export default shut;
