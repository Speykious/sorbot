import {
    ActionRowBuilder,
    MessageActionRowComponentBuilder,
    ModalActionRowComponentBuilder,
    ModalBuilder,
    ModalSubmitInteraction,
    TextInputBuilder,
    TextInputStyle,
} from 'discord.js';
import SuUser from '../../database/models/SuUser';
import { sendConfirmationEmail } from '../../email/smtp';
import {
    generateCode,
    isEmail,
    getSorbonneDomain,
} from '../../email/validation';
import {
    errorEmbed,
    singleEmbedMessage,
    successEmbed,
    warningEmbed,
} from '../../embeds';
import hash, { decrypt, encrypt } from '../../encryption';
import { ModalSubhandler } from '../../interfaces';
import { dislog } from '../../logging';
import cancel from '../components/cancel';
import enterCode from '../components/enterCode';
import resendCode from '../components/resendCode';
import { getSuUserFromInteraction } from '../helpers';

/** `emailAddressForm` modal interaction metadata. */
const data = new ModalBuilder()
    .setCustomId('emailAddressForm')
    .setTitle('Vérification')
    .addComponents(
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
            new TextInputBuilder()
                .setCustomId('email')
                .setLabel('Votre adresse mail Sorbonne')
                .setPlaceholder('name.surname@etu.sorbonne-universite.fr')
                .setStyle(TextInputStyle.Short)
                .setRequired(true)
        )
    );

/** `emailAddressForm` modal interaction handler. */
async function run(interaction: ModalSubmitInteraction) {
    const { user, fields } = interaction;

    await interaction.deferReply({ ephemeral: true });
    const email = fields.getTextInputValue('email');

    if (!isEmail(email)) {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Entrée invalide')
                    .setDescription(`**${email}** n'est pas une adresse mail.`)
            )
        );
        return;
    }

    if (!(await getSorbonneDomain(email))) {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Entrée invalide')
                    .setDescription(
                        `**${email}** n'est pas une adresse mail de la Sorbonne.`
                    )
            )
        );
        return;
    }

    const suUser = await getSuUserFromInteraction(interaction, {
        touchUser: true,
        editReply: true,
    });
    if (!suUser) return;

    // Make sure the email address is not already in use
    // NOTE: Since email addresses are only hashed when the user is verified,
    //       it means that the address is not considered in use if an unverified
    //       member has it.
    const sameEmailUser = await SuUser.findOne({
        where: { email: await hash(email) },
        attributes: ['id'],
    });

    if (sameEmailUser) {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Adresse déjà utilisée')
                    .setDescription(
                        `L'adresse mail **${email}** est déjà utilisée par un autre membre.`
                    )
            )
        );

        const sameEmailUserId = decrypt(sameEmailUser.id);

        // Report unique constraint warning
        dislog(
            'warning',
            singleEmbedMessage(
                warningEmbed()
                    .setTitle('UNIQUE CONSTRAINT WARNING')
                    .setDescription(
                        `User <@!${user.id}> (__${user.id}__) tried to use an email address which is already used!`
                    )
                    .addFields({
                        name: 'Current user of the address',
                        value: `<@!${sameEmailUserId}> (__${sameEmailUserId}__)`,
                    })
            )
        );
        return;
    }

    const code = generateCode();
    await suUser.update({
        email: encrypt(email),
        code,
    });

    await sendConfirmationEmail(email, user, code);

    await interaction.editReply({
        embeds: [
            successEmbed()
                .setTitle('Adresse mail valide !')
                .setDescription(
                    'Nous vous avons envoyé un code de confirmation.'
                ),
        ],
        components: [
            new ActionRowBuilder<MessageActionRowComponentBuilder>().addComponents(
                enterCode.data,
                resendCode.data,
                cancel.data
            ),
        ],
    });
}

/**
 * Form where the user will enter their email address.
 * If the user is unverified and provides a valid address from Sorbonne,
 * an email with a confirmation code is sent to them and they can either:
 * - enter the confirmation code,
 * - send a new confirmation code,
 * - cancel the verification process.
 */
const emailAddressForm: ModalSubhandler = { data, run };
export default emailAddressForm;
