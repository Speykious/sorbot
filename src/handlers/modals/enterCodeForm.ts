import {
    ActionRowBuilder,
    ModalActionRowComponentBuilder,
    ModalBuilder,
    ModalSubmitInteraction,
    TextInputBuilder,
    TextInputStyle,
} from 'discord.js';
import { singleEmbedMessage, successEmbed, errorEmbed } from '../../embeds';
import { ModalSubhandler } from '../../interfaces';
import { getSuUserFromInteraction, verifyUser } from '../helpers';

/** `enterCodeForm` modal interaction metadata. */
const data = new ModalBuilder()
    .setCustomId('enterCodeForm')
    .setTitle('Code de vérification')
    .addComponents(
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
            new TextInputBuilder()
                .setCustomId('code')
                .setLabel('Code qui vous a été envoyé')
                .setPlaceholder('BRUh69')
                .setStyle(TextInputStyle.Short)
                .setRequired(true)
        )
    );

/** `enterCodeForm` modal interaction handler. */
async function run(interaction: ModalSubmitInteraction) {
    const { user, fields } = interaction;

    await interaction.deferReply({ ephemeral: true });

    const suUser = await getSuUserFromInteraction(interaction, {
        editReply: true,
    });
    if (!suUser) return;

    const code = fields.getTextInputValue('code').trim();
    if (!suUser.email)
        throw new Error('A user without email used the `enterCodeForm`');

    if (suUser.code === code) {
        await verifyUser(user, suUser);

        await interaction.editReply(
            singleEmbedMessage(
                successEmbed()
                    .setTitle('Code valide !')
                    .setDescription('Vous êtes maintenant vérifié.e 👍')
            )
        );
    } else {
        await interaction.editReply(
            singleEmbedMessage(
                errorEmbed()
                    .setTitle('Code invalide.')
                    .setDescription("Le code n'est pas le bon. Réessayez.")
            )
        );
    }
}

/**
 * Form where the user will enter the confirmation code they received.
 * If the entered code is valid, they get verified.
 */
const enterCodeForm: ModalSubhandler = { data, run };
export default enterCodeForm;
