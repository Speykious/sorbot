Sorbot - Code

----------------

Bonjour {tag},

Dans Discord, cliquez sur le
bouton "Entrer le code" puis
renseignez le code suivant :

{code}

Si vous n'êtes pas originaire de cette opération,
vous pouvez ignorer ce mail ou y répondre en
incluant dans son contenu la phrase suivante :

"Je ne suis pas originaire de cette opération"

afin de nous reporter l'incident.
